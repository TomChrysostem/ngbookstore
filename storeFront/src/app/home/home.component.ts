import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/product.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public latest_products:any;
  public bestseller_products:any;
  constructor(private prodSvc:ProductService) { }

  ngOnInit() {
    this.prodSvc.getAll().subscribe((res:any)=>{
      console.log(res);
      this.latest_products = res.data;
    },
    (err)=>{
      console.log(err);
    });
    this.prodSvc.getBestSeller().subscribe((res:any)=>{
      console.log(res);
      this.bestseller_products = res.data;
    },
    (err)=>{
      console.log(err);
    });
    this.prodSvc.getLatest().subscribe((res:any)=>{
      console.log(res);
      this.latest_products = res.data;
    },
    (err)=>{
      console.log(err);
    });
  }

}
