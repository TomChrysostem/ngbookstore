import { NgModule } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { PromotionsComponent } from './promotions/promotions.component';
import { ContactComponent } from './contact/contact.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path:'home',component:HomeComponent},
  {path:'products',component:ProductsComponent},
  {path:'products/:id/edit',component:ProductsComponent},
  {path:'products/:id/view',component:ProductsComponent},
  {path:'products/:id/delete',component:ProductsComponent},
  {path:'promotions',component:PromotionsComponent},
  {path:'contact',component:ContactComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
