import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../services/category.service';
import { ProductService } from '../services/product.service';
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  public cats:any;
  public id:any;
  public latest_products:any;
  public product_detail:any;
  constructor(private catSvc: CategoryService,private prodSvc:ProductService) { }

  ngOnInit() {
    this.prodSvc.getOne(this.id).subscribe((res:any)=>{
      this.product_detail = res.data;
    }),
    this.catSvc.getAll().subscribe((res:any)=>{
      this.cats = res.data;
    });
    this.prodSvc.getAll().subscribe((res:any)=>{
      this.latest_products = res.data;
    },
    (err)=>{
      console.log(err);
    })

  }

}
