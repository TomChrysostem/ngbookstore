import { Injectable } from '@angular/core';
import{ HttpClient } from '@angular/common/http';
import { API_URL,PUBLIC_URL } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  constructor(private http: HttpClient) { 

  }
  getAll(){
    return this.http.get(API_URL+"/category");
  }
}
