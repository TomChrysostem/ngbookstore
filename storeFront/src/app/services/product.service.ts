import { Injectable } from '@angular/core';
import{ HttpClient } from '@angular/common/http';
import { API_URL,PUBLIC_URL } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(private http: HttpClient) { 

  }
  getAll(){
    return this.http.get(API_URL+"/product");
  }
  getOne(id){
    return this.http.get(API_URL+"/product/"+id);
  }
  getURL(img_name){
    return PUBLIC_URL+"uploads/"+img_name;
  }
  getBestSeller(){
    return this.http.get(API_URL+"/product");
  }
  getLatest(){
    return this.http.get(API_URL+"/product");
  }
}
