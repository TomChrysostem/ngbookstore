'use strict';

module.exports = (sequelize, DataTypes) => {
    
    var order_item = sequelize.define('Order_items',{
        order_id: DataTypes.INTEGER,
        product_id: DataTypes.INTEGER,
        qty:DataTypes.INTEGER,
        price:DataTypes.FLOAT,
        subtotal:DataTypes.FLOAT,
        status: DataTypes.INTEGER
    },
    {
        tableName:'Order_items',
        timestamps:true
    });
    return order_item;
}
