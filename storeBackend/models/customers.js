'use strict';

module.exports = (sequelize, DataTypes) => {
    
    var customer = sequelize.define('Customer',{
        name: DataTypes.STRING,
        phone: DataTypes.STRING,
        address: DataTypes.STRING,
        township_id: DataTypes.INTEGER,
        status: DataTypes.INTEGER
    },
    {
        tableName:'customers',
        timestamps:true
    });
    return customer;
}