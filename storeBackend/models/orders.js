'use strict';

module.exports = (sequelize, DataTypes) => {
    
    var order = sequelize.define('Order',{
        customer_id: DataTypes.INTEGER,
        total_amount: DataTypes.FLOAT,
        discount:DataTypes.FLOAT,
        payment_method:DataTypes.INTEGER,
        status: DataTypes.INTEGER
    },
    {
        tableName:'orders',
        timestamps:true
    });
    return order;
}
