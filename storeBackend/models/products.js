'use strict';

module.exports = (sequelize, DataTypes) => {
    
    var Product = sequelize.define('Product',{
        title: DataTypes.STRING,
        category_id: DataTypes.INTEGER,
        thumbnail: DataTypes.STRING,
        description: DataTypes.STRING,
        price: DataTypes.FLOAT
    },
    {
        tableName:'products',
        timestamps:true
    });

    Product.associate = function (models) {
        models.Product.belongsTo(models.Category, {
            onDelete: "CASCADE",
            foreignKey: "category_id"
    });
    
    }
    return Product;
}
