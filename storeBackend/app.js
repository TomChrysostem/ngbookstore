require('dotenv').config()
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var expressLayouts = require('express-ejs-layouts');
var cors = require('cors');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var productApiRouter = require('./routes/api/product');
var categoryApiRouter = require('./routes/api/category');
var customerApiRouter = require('./routes/api/customer');
var orderApiRouter = require('./routes/api/order');

var dashboardRouter = require('./routes/admin/dashboard');
var productAdminRouter = require('./routes/admin/product');
var categoryAdminRouter = require('./routes/admin/category');
var customerAdminRouter = require('./routes/admin/customer');
var orderAdminRouter = require('./routes/admin/order');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(expressLayouts);
app.set('layout','layouts/admin');

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.use('/api/product', productApiRouter);
app.use('/api/category', categoryApiRouter);
app.use('/api/customer', customerApiRouter);
app.use('/api/order', orderApiRouter);

app.use('/admin/product', productAdminRouter);
app.use('/admin/category', categoryAdminRouter);
app.use('/admin/customer', customerAdminRouter);
app.use('/admin/order', orderAdminRouter);
app.use('/admin/dashboard', dashboardRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
