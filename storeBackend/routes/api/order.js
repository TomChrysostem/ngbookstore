var express = require('express');
var router = express.Router();
var models = require('../../models');

/* GET home page. */
router.get('/', async function(req, res, next) {
  let orders = await models.Order.findAll();
  res.json({
  	success:true,
  	message:'Order success',
  	data:orders,
  	error: null
  })
});

module.exports = router;
