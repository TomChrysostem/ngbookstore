var express = require('express');
var router = express.Router();
var models = require('../../models');

/* GET home page. */
router.get('/', async function(req, res, next) {
  let products = await models.Product.findAll();
  res.json({
  	success:true,
  	message:'Product success',
  	data:products,
  	error: null
  })
});

module.exports = router;
