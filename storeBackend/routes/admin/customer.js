var express = require('express');
var router = express.Router();
var models = require('../../models');

/* GET home page. */
router.get('/', async function(req, res, next) {
  let customers = await models.Customer.findAll();
  res.json({
  	success:true,
  	message:'Customer success',
  	data:customers,
  	error: null
  })
});

module.exports = router;
