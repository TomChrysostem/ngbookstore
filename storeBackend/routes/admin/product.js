var express = require('express');
var router = express.Router();
var models = require('../../models');
var multer  = require('multer')
var upload = multer({ dest: 'public/uploads' })

/* GET home page. */
router.get('/', async function(req, res, next) {
  let products = await models.Product.findAll({include:[models.Category]});
  res.render('admin/product',{ title:'Express', products:products });
});

router.get('/create', async function(req, res, next){
	let categories = await models.Category.findAll();
	res.render('admin/products_create', { title:'Express', categories: categories});
});

router.post('/store',upload.single('thumbnail'),function(req,res,next){
	console.log('hello');
	let formData = req.body;
	let image = req.file;
	if(image){
		formData.thumbnail = image.filename;
	}
	models.Product.create(formData).then((err,result)=>{
		return res.redirect('/admin/product');
	}).catch(function(err){
		console.log(err);
});

// Delete Form
router.post('/:id/delete', [], (req, res, next ) => {
   let id = req.params.id;

   models.Product.destroy(
   		{where: {id: id}}
    ).then( (data) => {
    req.flash("success", "Successfully Deleted");
    return res.redirect('/');
   });
});

router.get('/:id',async function(req,res,next){
	let id = req.params.id;
	let product = await models.Product.findOne({where:{id:id}});
	res.json({
		success : true,
		message : 'Product retrived success',
		data : product,
		errors : null
	})
});

})

module.exports = router;
