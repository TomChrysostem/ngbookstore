var express = require('express');
var router = express.Router();
var models = require('../../models');

/* GET home page. */
router.get('/', async function(req, res, next) {
  let orderitems = await models.Order_items.findAll();
  res.json({
  	success:true,
  	message:'Order_items success',
  	data:orderitems,
  	error: null
  })
});

module.exports = router;
