var express = require('express');
var router = express.Router();
var models = require('../../models');

/* GET home page. */
router.get('/', async function(req, res, next) {
  let categories = await models.Category.findAll();
  res.json({
  	success:true,
  	message:'Category success',
  	data:categories,
  	error: null
  })
});

module.exports = router;
