var express = require('express');
var router = express.Router();
var models = require('../../models');

/* GET home page. */
router.get('/', async function(req, res, next) {
  res.render('admin/dashboard',{title:'Express'});
});

module.exports = router;
